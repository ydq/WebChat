//函数防抖
function debounce(n,u){var l,t,e=null,i=function(){n.apply(l,t),e=null};return function(){l=this,t=arguments,e&&(clearTimeout(e),e=null),e=setTimeout(i,u)}}
//解析消息中的表情符号和可能性的网址
function resolveMsg(txt) {return txt.replace(/\[#(\d+)\]/g, function(a,b){return b < 95 ? '<img class="iface" src="/images/face/' + b + '.png">': '';}).replace(/(https?:\/\/[\w]+[\w\.\/%#:]+)/g,'<a href="$1" target="_blank">$1</a>')}
//js图片压缩
function compressImg(imgData, callback) {if (!imgData)return;var canvas = document.createElement('canvas');var img = new Image();img.onload = function() {if (img.height > 200) {img.width *= 200 / img.height;img.height = 200;}var ctx = canvas.getContext("2d");ctx.clearRect(0, 0, canvas.width, canvas.height);canvas.width = img.width;canvas.height = img.height;ctx.drawImage(img, 0, 0, img.width, img.height);callback && callback(canvas.toDataURL("image/jpeg"));};img.src = imgData;}
