package work.darren.webchat.bean;

import javax.websocket.Session;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author darren
 * @date 2016年7月15日 下午4:28:36
*/
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class ChatUser {

    String name;
	@JSONField(serialize=false)
    Session session;

	
}
