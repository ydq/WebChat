package work.darren.webchat.bean;

public enum ChatType {
    login, notify, msg, img, location, music
}
