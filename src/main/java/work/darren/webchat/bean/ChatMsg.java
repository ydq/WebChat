package work.darren.webchat.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author darren
 * @date 2016年7月15日 下午11:14:12
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ChatMsg {
    ChatType type;
    @JSONField(format = "HH:mm")
    LocalTime time = LocalTime.now();
    String uid;
    String msg;
    String name;
    String url;

    public ChatMsg(String uid) {
        this.uid = uid;
    }
}
