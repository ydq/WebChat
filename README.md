# WebChat 网页即时多人聊天工具 

### 一款基于HTML5的WebSocket技术实现的界面模仿微信的网页多人即时聊天工具，整个项目没什么技术含量，前后端写的也很烂，但是可以作为一个半成品接着开发或者作为学习的参考。

### 已实现的功能和特色

- 使用vw和rem作为尺寸单位，大小屏幕自动缩放适应（ipad自动等比例放大）
- 根据昵称自动生成类似于GitHub的identicon头像
- 支持表情发送，表情来自于QQ默认表情
- 支持本地聊天记录存储（最多50条，由原来的sessionStorage修改为永久的localStorage）
- 支持发送图片（因为服务器不保存任何聊天内容，所以图片本地会做一个js压缩然后传输采用的base64的DataURL，走websocket通道防止消息过长引发异常，所以清晰度比较低，但可以防止成为图床）
- 支持分享音乐（音乐由网易云音乐提供）
- 支持分享地理位置（需定位权限，需部署HTTPS）
- ios支持固定到主屏幕更像一个原生APP
- Android支持消息震动

### 未实现（懒得实现）的功能和已知的问题

- [@]用户 功能
- 因为不涉及到数据库，所以用户ID直接采用当前websocket的session id 进行Hash编码后返回作为用户ID，因此当刷新后可能看到自己的历史消息显示为白色且在左边（类似于其他用户发送的消息），这个不采用数据库做用户模块的话暂时想不到更好的方法
- 图片可以额外使用ajax做上传，保存到服务器，同时服务器来生成缩略图和原图，然后返回图片URL，聊天消息中的图片点击可以做查看大图/原图的功能
- 手动清屏
- [HTML5录音的Api](http://caniuse.com/#feat=mediarecorder)在手机端均不支持，所以本想做一个和微信一样发送语音的功能也暂告失败
- PC版本想界面独立做，因为现在的界面其实是手机版的界面，采用rem和vm根据屏幕等比例缩放，不适合于PC端
- 查看在线用户列表和一对一私聊功能

### 使用的一些第三方的库

- [identicon.js](https://github.com/stewartlord/identicon.js) 生成类似于GitHub默认头像的js库
- [iscroll](https://github.com/cubiq/iscroll)  纵享丝滑的触摸滚动库
- [laytpl](https://github.com/sentsin/laytpl)  小而美的js模板库
- sha1加密库 (出处忘了是哪了，如果有知道的我可以补上，作用是将昵称加密后生成的hash值给identicon去生成头像)
- [zepto](https://github.com/madrobby/zepto)  用极小的体积呈现一个jQuery的完美替代
- [fastjson](https://github.com/alibaba/fastjson)  后台Java的Json生成和解析库，来自于阿里巴巴



### 项目打开方式

下载项目，导入IDE（IDE需要安装lombok支持），直接使用maven jetty插件来启动（jetty:run）

#### 最后

做这个小demo项目是看到有人在开源中国的动弹上发了一个简易的websocket聊天工具，界面产不忍赌，而自己做了很久的web开发，但从没尝试过html5的websocket，所以自己就想弄一个玩玩顺便理解一下相关的技术。主要是HTML5的API很多，但是移动端真正支持的却比较少。
